﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellsFactory : MonoBehaviour
{

    List<Zone> cells = new List<Zone>();

    public Zone GetZone(Vector2 position)
    {
        return cells.Find(item => item.position == position);
    }

    public Zone CreateCell(Vector2 position, GameObject obj)
    {

        Zone zone = new Zone(); // gameObject.AddComponent<GameCell>();

        zone.position = position;


        zone.settings = CellOptions.Reward | CellOptions.Defender;

        

        if (Random.value < RandomChanse.chanseNonAccessible)
        {
            print("NonAccessible");
            zone.settings = CellOptions.NonAccessible;
        }

        if (position.y == 0)
        {
            zone.settings = CellOptions.Empty;
        }

        
        
        if ((zone.settings & CellOptions.NonAccessible) > 0)
        {
            print("ZoneBlocked");
            zone.zoneType = new ZoneBlocked();
        } else
        {
            zone.zoneType = new ZoneAllowed();
        }

        if ((zone.settings & CellOptions.Reward) > 0)
        {
            Crystal crystal = new Crystal();
            crystal.elemental = RandomChanse.RandomElemental();
             zone.reward = crystal;
        }


        if ((zone.settings & CellOptions.Defender) > 0)
        {
            if (Random.value < RandomChanse.chanseNoDefender)
            {
                zone.settings ^= CellOptions.Defender;
                zone.defender = new NoMonster();
            }
            else
            {
                ElementalMonster monster = new ElementalMonster();
                monster.elemental = RandomChanse.RandomElemental();
                zone.defender = monster;
            }
        } else
        {
            zone.defender = new NoMonster();
        }
            


        cells.Add(zone);
        
        return zone;
    }


}

static class RandomChanse
{
    
    public static double chanseNonAccessible = 0.05;
    public static double chanseNoDefender = 0.05;

    public static ElementType RandomElemental()
    {
        return (ElementType) (Random.value * 6);

    }

}