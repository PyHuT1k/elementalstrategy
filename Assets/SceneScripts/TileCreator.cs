﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCreator : MonoBehaviour
{
    private float height, width;
    public int columns, rows;
    public GameObject prefab;
    CellsFactory factory;
    Dictionary<Vector2, GameObject> cells = new Dictionary<Vector2, GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        //fillBattleField();


    }

    public void fillBattleField(CellActivityHandler activityHandler)
    {
        Canvas canvas = this.gameObject.GetComponent<Canvas>();
        factory = gameObject.AddComponent<CellsFactory>();
        RectTransform transform = this.gameObject.GetComponent<RectTransform>();
        //    print("transform rect " + transform.rect);

        RectTransform prefabTransform = prefab.GetComponent<RectTransform>();

        float scale = transform.rect.width / prefabTransform.rect.width / columns;
        print("transform scale " + scale);
        float startHeight = transform.position.y;
        float startWidth = transform.position.x - (transform.rect.width * transform.pivot.x);

        width = prefabTransform.rect.width * scale;
        height = prefabTransform.rect.height * scale;

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                Vector2 position = new Vector2(j, i);
                Zone zone = factory.CreateCell(position, prefab);


                prefab.GetComponent<GameCell>().position = position;

                GameObject instanse = Instantiate(prefab, new Vector3(startWidth + (width / 2) + j * width,
                                                                      startHeight + (height / 2) + (i * height),
                                                                      0),
                                                                      Quaternion.identity);
                instanse.transform.localScale += new Vector3(scale - instanse.transform.localScale.x, scale - instanse.transform.localScale.y, 0);

                cells[position] = instanse;
                instanse.GetComponent<GameCell>().zoneModel = zone;
                instanse.GetComponent<GameCell>().activityHandler = activityHandler;
            }
        }
    }

    public GameObject getCell(Vector2 position)
    {
        return cells[position];

    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
