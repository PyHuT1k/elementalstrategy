﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;

public enum ElementType
{
    Red,
    Yellow,
    Blue,
    Purple,
    Green,
    Orange
}

public static class Extensions
{
    public static Color ElementColor(this ElementType element)
    {

        switch (element)
        {
            case ElementType.Red:
                return Color.red;
            case ElementType.Blue:
                return Color.blue;
            case ElementType.Green:
                return Color.green;
            case ElementType.Orange:
                return Color.magenta;
            case ElementType.Purple:
                return Color.cyan;
            case ElementType.Yellow:
                return Color.yellow;
            default:
                break;
        }

        return Color.clear;
    }

    public static ElementType Superior(this ElementType element)
    {
        switch (element)
        {
            case ElementType.Red:
                return ElementType.Blue;
            case ElementType.Blue:
                return ElementType.Yellow;
            case ElementType.Green:
                return ElementType.Orange;
            case ElementType.Orange:
                return ElementType.Purple;
            case ElementType.Purple:
                return ElementType.Green;
            case ElementType.Yellow:
                return ElementType.Red;
            default:
                throw new NotImplementedException();
                
        }
    }

    public static int compareElementPower(this ElementType first, ElementType second)
    {
       if (first.Superior() == second)
        {
            return 0;
        }
        if (second.Superior() == first)
        {
            return 2;
        }

        return 1;
    }
}


public enum CellOptions
{
    Empty = 0 ,
    NonAccessible = 1 << 0,
    Reward = 1 << 1,
    Defender = 1 << 2
}

public enum MosterType
{
    None,
    Elemental

}

public enum RewardType
{
    None,
    ElementalCrystal

}

public class Monster : CellDefender
{
    public MosterType type = MosterType.Elemental;

    void ObjectConfigurator.Configure(GameObject gameObject)
    {
        MonsterConfigure(gameObject);
    }

    protected virtual void MonsterConfigure(GameObject gameObject)
    {
        throw new NotImplementedException();
    }

    void CellDefender.FightWith(Player player)
    {
        MonsterFightWith(player);
    }

    protected virtual void MonsterFightWith(Player player)
    {
        
    }
}

public class NoMonster : CellDefender
{
    void ObjectConfigurator.Configure(GameObject gameObject)
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.clear;
    }

    void CellDefender.FightWith(Player player)
    {
        
    }
}

public class ElementalMonster : Monster
{
    public ElementType elemental;
    GameObject relatedGameObject;

    protected override void MonsterConfigure(GameObject gameObject)
    {
        relatedGameObject = gameObject;
        gameObject.GetComponent<SpriteRenderer>().color = elemental.ElementColor();
    }

    protected override void MonsterFightWith(Player player)
    {
        int damage = elemental.compareElementPower(player.elemental);
        player.GetDamage(damage);
        player.elemental = elemental;
        relatedGameObject.GetComponent<SpriteRenderer>().color = Color.clear;
    }
}



public class Crystal : CellReward
{
    public ElementType elemental;
    GameObject relatedGameObject;
    void CellReward.Collected()
    {
        relatedGameObject.GetComponent<SpriteRenderer>().color = Color.clear;
    }

    void ObjectConfigurator.Configure(GameObject gameObject)
    {
        relatedGameObject = gameObject;
        gameObject.GetComponent<SpriteRenderer>().color = elemental.ElementColor();
    }

    RewardType CellReward.Type()
    {
        return RewardType.ElementalCrystal;
    }
}

public class ZoneBlocked : CellZone
{

    void ObjectConfigurator.Configure(GameObject gameObject)
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.black;
    }

    bool CellZone.IsAccessible()
    {
        return false;
    }
}

public class ZoneAllowed : CellZone
{

    void ObjectConfigurator.Configure(GameObject gameObject)
    {
        gameObject.GetComponent<SpriteRenderer>().color = Color.white;
    }

    bool CellZone.IsAccessible()
    {
        return true;
    }
}




public interface JustObjectConfigurator : ObjectConfigurator
{

}


public interface CellDefender : ObjectConfigurator
{
    void FightWith(Player player);
}

public interface CellZone : ObjectConfigurator
{
    bool IsAccessible();
}

public interface CellReward : ObjectConfigurator
{
    RewardType Type();
    void Collected();
}


public class Zone
{
    public CellOptions settings = CellOptions.Reward | CellOptions.Defender;
    public CellDefender defender;
    public CellZone zoneType;
    public CellReward reward;
    public Vector2 position;
}



public class GameCell : MonoBehaviour
{

    public GameObject monster;
    public GameObject zone;
    public GameObject reward;
    //   [SerializeField]
    public Zone zoneModel;
    public Vector2 position;

    public CellActivityHandler activityHandler;

    // Start is called before the first frame update
    void Start()
    {
        SpriteRenderer mR = monster.GetComponent<SpriteRenderer>();
        //     print("position " + position);

        //     print(position + "get");
        //    print("ElementColor " + defender.elemental);// ElementColor(defender.elemental));
        zoneModel.defender.Configure(monster);
        zoneModel.zoneType.Configure(zone);
        zoneModel.reward.Configure(reward);




    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            print(position + " touch");
        }
    }

    void OnTouchUp()
    {
        //do other touch code but when pulling finger off of touchscreen
        print(position + " touch");
    }

    void OnMouseDown()
    {
        // Destroy the gameObject after clicking on it
        activityHandler.touchedCell(position);
    }

    void OnClick()
    {
        print(position + " OnClick");
    }


}
