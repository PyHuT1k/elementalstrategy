﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface ObjectConfigurator
{
    void Configure(GameObject gameObject);
}

public class Player  
{
    public int heathPoints = 10;
    public Inventory inventory = new Inventory();
    public ElementType elemental;
    public Vector2 position;

    public void Configure(GameObject gameObject)
    {
        gameObject.GetComponent<SpriteRenderer>().color = elemental.ElementColor();
    }


    public List<Vector2> possiblePositions()
    {
        List<Vector2> result = new List<Vector2>();
        result.Add(new Vector2(position.x -1 , position.y + 1));
        result.Add(new Vector2(position.x , position.y + 1));
        result.Add(new Vector2(position.x + 1, position.y + 1));
        return result;
    }

    public void FightWith(CellDefender defender)
    {

    }


    public void Collect(CellReward reward)
    {
        var rewardType = reward.Type();
        switch (rewardType)
        {
            case (RewardType.ElementalCrystal):
                {
                    var crystal = reward as Crystal;
                    inventory.Add(crystal);

                }
                break;
            case (RewardType.None):
                {

                }
                break;
        }
        reward.Collected();
    }

    public void GetDamage(int damage)
    {
        heathPoints -= damage;
    }



}

public class Inventory
{
    public List<InventoryCell> cells = new List<InventoryCell>();

    public void Add(Crystal crystal)
    {
        var cell = cells.Find(item => FindElementCell(item, crystal.elemental));
        
        if (cell == null)
        {
            cell = new InventoryCell();
            var content = new ElementalCellContent();
            content.element = crystal.elemental;
            cell.content = content;
            cell.order = cells.Count;
            cells.Add(cell);
        }

        cell.count += 1;

    }

    private static bool FindElementCell(InventoryCell cell, ElementType element)
    {

        var content = cell.content as ElementalCellContent;
        if (content.element == element)
        {
            MonoBehaviour.print("FindElementCell true");
            return true;
        }
        MonoBehaviour.print("FindElementCell false");
        return false;
    }
}

public interface InventoryCellContent : ObjectConfigurator
{
    
}

public class ElementalCellContent : InventoryCellContent
{
    public ElementType element;

    void ObjectConfigurator.Configure(GameObject gameObject)
    {
        gameObject.GetComponent<Text>().color = element.ElementColor();
      
    }
}

public class InventoryCell 
{
    public int order;
    public InventoryCellContent content;
    public int count;

    public void Configure(GameObject gameObject)
    {
        MonoBehaviour.print("InventoryCell Configure");
        gameObject.GetComponent<Text>().text = "C " + count;
        content.Configure(gameObject);
    }
}



public class GameSettings
{
    public Vector2 startPosition = new Vector2(2, 0);
}

public interface CellActivityHandler
{
    void touchedCell(Vector2 position);
}

public class GameEngine : MonoBehaviour, CellActivityHandler
{
    public GameObject userInterface;
    public GameObject map;
    public GameObject playerPrefab;
    private GameObject player;
    public Player playerModel = new Player();
    public GameSettings settings = new GameSettings();

    private UserPanel userPanel
    {
        get
        {
            return userInterface.GetComponent<UserPanel>();
        }

    }

    private TileCreator tileCreator {
        get
        {
           return map.GetComponent<TileCreator>();
        }
        
        }

    // Start is called before the first frame update
    void Start()
    {
        tileCreator.fillBattleField(this);

        player = Instantiate(playerPrefab, new Vector3(0, 0, -4), Quaternion.identity);
        movePlayerTo(settings.startPosition);
        fillInventory();
        fillHealth();
        playerModel.Configure(player);
    }

    public void movePlayerTo(Vector2 position)
    {
        GameObject cell = tileCreator.getCell(position);
        playerModel.position = position;
        player.transform.position = new Vector3(cell.transform.position.x, 
                                                cell.transform.position.y,
                                                -4) ;

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void CellActivityHandler.touchedCell(Vector2 position)
    {
        if (!playerModel.possiblePositions().Contains(position))
        {
            return;
        }
        

        GameCell cell = tileCreator.getCell(position).GetComponent<GameCell>();

        if (!cell.zoneModel.zoneType.IsAccessible() )
        {
            return;
        }

        cell.zoneModel.defender.FightWith(playerModel);
        var reward = cell.zoneModel.reward;
        playerModel.Collect(reward);
        
        

        movePlayerTo(position);
        playerModel.Configure(player);

        fillInventory();
        fillHealth();
    }

    void fillHealth()
    {
        userPanel.health.GetComponent<Text>().text = "H " + playerModel.heathPoints;
    }

    void fillInventory()
    {
        foreach (InventoryCell cell in playerModel.inventory.cells)
        {
            cell.Configure(userPanel.inventory[cell.order]);
        }
    }


    

}
